
package com.example.ankir.weatherforecast.model;

import java.util.HashMap;
import java.util.Map;

public class Astronomy {

    public String sunrise;
    public String sunset;
    public String moonrise;
    public String moonset;

}
