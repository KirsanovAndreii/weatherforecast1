
package com.example.ankir.weatherforecast.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Weather {

    public String date;
    public List<Astronomy> astronomy = null;
    public String maxtempC;
    public String maxtempF;
    public String mintempC;
    public String mintempF;
    public String totalSnowCm;
    public String sunHour;
    public String uvIndex;
    public List<Hourly> hourly = null;

}
