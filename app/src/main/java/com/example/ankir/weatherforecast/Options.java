package com.example.ankir.weatherforecast;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Options extends AppCompatActivity {

    private static String cityCurrent;
    @BindView(R.id.city_current)
    EditText cityCurrentEdit;

    private static boolean isCelsium;
    @BindView( R.id.isCelsium)
    RadioButton radioButtonC;
    @BindView( R.id.isFahrenheit)
    RadioButton radioButtonF;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        cityCurrentEdit.setText(intent.getStringExtra(MainActivity.KEY_CITY_SP));
        if (intent.getBooleanExtra(MainActivity.KEY_SCALE_SP, true)) {
            radioButtonC.setChecked(true);
            radioButtonF.setChecked(false);
        }
        else { radioButtonC.setChecked(false);
            radioButtonF.setChecked(true);}
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences sp = getSharedPreferences(MainActivity.SP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(MainActivity.KEY_CITY_SP, cityCurrentEdit.getText().toString());
        editor.putBoolean(MainActivity.KEY_SCALE_SP, radioButtonC.isChecked());
        editor.commit();

    }
}
