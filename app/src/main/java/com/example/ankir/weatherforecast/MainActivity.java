package com.example.ankir.weatherforecast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ankir.weatherforecast.model.ModelForAdapter;
import com.example.ankir.weatherforecast.model.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;

public class MainActivity extends AppCompatActivity {
    List<ModelForAdapter> listWeather5day = new ArrayList<>(); // погода на 5 дней все данные

    AdapterList adapterList;
    public static final String KEY_WEATHER = "key_weather";
    public static final String SP_NAME = "weather_save";
    public static final String KEY_WEATHER_SP = "key_weather_sp";
    public static final String KEY_TIME_SP = "key_time";
    public static final String KEY_CITY_SP = "key_city";
    public static final String KEY_SCALE_SP = "key_scale";

    public static final int NOTIFY_ID = 100;
    public static final int ID = 200;
    public static String currentCity = "Kharkiv";
    public static boolean typeTempIsCelsium = true;
    public static final String typeTempC = "C";
    public static final String typeTempF = "F";

    @BindView(R.id.am_list_view)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("Weather Forecast " + currentCity);
        adapterList = new AdapterList(this, listWeather5day);
        listView.setAdapter(adapterList);

        readData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ActivityWeatherFor1Day.class);
                intent.putExtra(KEY_WEATHER, (ModelForAdapter) listWeather5day.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        if (currentCity.equals(sp.getString(KEY_CITY_SP, "Kharkiv")))
            readData();
        else {
            currentCity = sp.getString(KEY_CITY_SP, "Kharkiv");
            loadData();
        }
        setTitle("Weather Forecast " + currentCity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this, Options.class);
                intent.putExtra(KEY_CITY_SP, currentCity);
                intent.putExtra(KEY_SCALE_SP, typeTempIsCelsium);
                startActivity(intent);
                return true;
            case R.id.action_upload:
                loadData();
                return true;
            default:
        }
        return super.onOptionsItemSelected(item);
    }


    // создаем список данных
    public List<ModelForAdapter> createListweather(Response response) {
        List<ModelForAdapter> rezult = new ArrayList<>();

        for (int i = 0; i < response.data.weather.size(); i++) {
            ModelForAdapter model = new ModelForAdapter();
            model.setDate(response.data.weather.get(i).date);
            model.setIcon_wether(response.data.weather.get(i).hourly.get(0).weatherIconUrl.get(0).value);
            model.setWeather(response.data.weather.get(i).hourly.get(0).lang_ru.get(0).value);//.weatherDesc.
            model.setTemperatureC(response.data.weather.get(i).hourly.get(0).tempC + typeTempC);
            model.setTemperature_howfeelingC(response.data.weather.get(i).hourly.get(0).FeelsLikeC + typeTempC);
            model.setTemperatureF(response.data.weather.get(i).hourly.get(0).tempF + typeTempF);
            model.setTemperature_howfeelingF(response.data.weather.get(i).hourly.get(0).FeelsLikeF + typeTempF);
            model.setAtm_pressure(response.data.weather.get(i).hourly.get(0).pressure);
            model.setSunrise(response.data.weather.get(i).astronomy.get(0).sunrise);
            model.setSunset(response.data.weather.get(i).astronomy.get(0).sunset);
            model.setMoonrise(response.data.weather.get(i).astronomy.get(0).moonrise);
            model.setMoonset(response.data.weather.get(i).astronomy.get(0).moonset);
            model.setHumidity(response.data.weather.get(i).hourly.get(0).humidity);//влажность
            model.setCloudcover(response.data.weather.get(i).hourly.get(0).cloudcover);// облачность
            model.setChanceofrain(response.data.weather.get(i).hourly.get(0).chanceofrain);//вероятность дождя
            model.setChanceoffog(response.data.weather.get(i).hourly.get(0).chanceoffog);// вероятность тумана
            model.setWind(response.data.weather.get(i).hourly.get(0).windspeedKmph);
            rezult.add(model);
            //       rezult.add(new ModelForAdapter("op", "Солнечно", "+" + (20 + i * 2), "+" + (25 + i * 3), "756",
            //             "04:30 AM", "08:47 PM", "01:05 PM", "12:31 AM",  "60", "4",  "43", "0", "8" ));
        }
        return rezult;
    }

    // закачиваем данные с интернета
    public void loadData() {
        Retrofit.getWeather(new Callback<Response>() {
            @Override
            public void success(Response response, retrofit.client.Response response2) {
                Toast.makeText(MainActivity.this, "Load data - " + currentCity, Toast.LENGTH_SHORT).show();
                //       Log.d("Okkk",response.toString());
                listWeather5day.clear();
                listWeather5day.addAll(createListweather(response));
                adapterList.notifyDataSetChanged();
                saveData();
                showNitification(listWeather5day.get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Load data - error", Toast.LENGTH_SHORT).show();
            }
        }, currentCity);
    }


    //запись данных на диск
    public void saveData() {
        SharedPreferences sp = getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String json = new Gson().toJson(listWeather5day);
        editor.putString(KEY_TIME_SP, System.currentTimeMillis() + "");
        editor.putString(KEY_WEATHER_SP, json);
        editor.putString(KEY_CITY_SP, currentCity);
        editor.putBoolean(KEY_SCALE_SP, typeTempIsCelsium);
        editor.commit();
        Toast.makeText(MainActivity.this, "Save data - ok", Toast.LENGTH_SHORT).show();
    }
    //ч

    //чтение данных с  диска
    public void readData() {
        SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        currentCity = sp.getString(KEY_CITY_SP, "Kharkiv");
        typeTempIsCelsium = sp.getBoolean(KEY_SCALE_SP, true);

        String time = sp.getString(KEY_TIME_SP, null);
        //     Log.d("time-------", time);
        if (time == null) {  // если файла еще нет - скачиваем с интеренета
            loadData();
        } else if (System.currentTimeMillis() - Long.parseLong(time) < 86400000) { // если не пришло время обновления - читаем с диска
//24 часа = 86 400 000млс

            String json = sp.getString(KEY_WEATHER_SP, null);
            //     Log.d("json----------", json);
            Type listType = new TypeToken<ArrayList<ModelForAdapter>>() {
            }.getType();
            List<ModelForAdapter> rezult = new Gson().fromJson(json.toString(), listType);
            listWeather5day.clear();
            listWeather5day.addAll(rezult);
            adapterList.notifyDataSetChanged();
            showNitification(listWeather5day.get(0));
            Toast.makeText(MainActivity.this, "READ data -- ok", Toast.LENGTH_SHORT).show();
        } else {  // если время обновления пришло - скачиваем с интеренета
            loadData();
        }
    }

    public void showNitification(ModelForAdapter modelOneDay) {
        String t;
        if (typeTempIsCelsium) t = modelOneDay.getTemperatureC();
        else t = modelOneDay.getTemperatureF();
        final String noticTemperatura = t;
        Intent notificationIntent = new Intent(this, MainActivity.class);//  -описывается экшн
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                ID, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        Picasso.with(this)
                .load(modelOneDay.getIcon_wether())
                .placeholder(R.drawable.i)
                .error(R.drawable.i)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        showNotify(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        showNotify(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }

                    Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);//  -описывается экшн
                    PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.this,
                            ID, notificationIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT);
                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    void showNotify(Bitmap bitmap) {
                        Notification.Builder builder = new Notification.Builder(MainActivity.this);
                        builder.setSmallIcon(R.drawable.i)
                                .setAutoCancel(true)        //                             автозакрытие уведомления после тапания
                                .setContentTitle("Weather of " + currentCity)    //                             заголовок
                                .setContentText("Температура " + noticTemperatura)            //              текс
                                .setLargeIcon(bitmap)  // иконка из файла  показывается один раз в момент создания уведомл(напр текст смс)
                                .setTicker("Ticker!");
                        Notification n = builder.build();
                        nm.notify(NOTIFY_ID, n);
                    }
                });

//        Notification.Builder builder = new Notification.Builder(this);
//        builder.setSmallIcon(R.drawable.i)
//               .setAutoCancel(true)        //                             автозакрытие уведомления после тапания
//                .setContentTitle("Weather of Kharkiv")    //                             заголовок
//                .setContentText("Температура "+ modelOneDay.getTemperature()+ typeTemp)            //              текс
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))  // иконка из файла  показывается один раз в момент создания уведомл(напр текст смс)
//                .setTicker("Ticker!")  ;
        //     .addAction(R.mipmap.ic_launcher, "Action", contentIntent);    3 кнопки(иконка, назв, интент) х3

//        Notification n = builder.build();
        //      nm.notify(NOTIFY_ID, n);

    }
}


