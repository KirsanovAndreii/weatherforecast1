package com.example.ankir.weatherforecast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ankir.weatherforecast.model.ModelForAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityWeatherFor1Day extends AppCompatActivity {
    ModelForAdapter weatherOneDay;

    @BindView(R.id.second_icon_wether)
    ImageView icon_wether;
    @BindView(R.id.second_weather)
    TextView weather;
    @BindView(R.id.second_temperature)
    TextView temperature;
    @BindView(R.id.second_temperature_howfeeling)
    TextView temperature_howfeeling;
    @BindView(R.id.second_atm_pressure)
    TextView atm_pressure;
    @BindView(R.id.second_sun_rise)
    TextView sun_rise;
    @BindView(R.id.second_sun_set)
    TextView sun_set;
    @BindView(R.id.second_moon_rise)
    TextView moon_rise;
    @BindView(R.id.second_moon_set)
    TextView moon_set;
    @BindView(R.id.second_humidity)
    TextView humidity;
    @BindView(R.id.second_cloudcover)
    TextView cloudcover;
    @BindView(R.id.second_chanceofrain)
    TextView chanceofrain;
    @BindView(R.id.second_chanceoffog)
    TextView chanceoffog;
    @BindView(R.id.second_wind)
    TextView wind;
    @BindView(R.id.date)
    TextView date;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_for1_day);
        ButterKnife.bind(this);
        setTitle("Weather Forecast " + MainActivity.currentCity);
        Intent intent = getIntent();
        weatherOneDay = (ModelForAdapter) intent.getSerializableExtra(MainActivity.KEY_WEATHER);
        //    icon_wether.setImageResource(R.drawable.i);

        Picasso.with(this)
                .load(weatherOneDay.getIcon_wether())
                .placeholder(R.drawable.i)
                .error(R.drawable.i)
                .into(icon_wether);


        weather.setText(weatherOneDay.getWeather());
        if (MainActivity.typeTempIsCelsium) {
            temperature.setText("Температура " + weatherOneDay.getTemperatureC());
            temperature_howfeeling.setText("По ощущениям " + weatherOneDay.getTemperature_howfeelingC());
        } else {
            temperature.setText("Температура " + weatherOneDay.getTemperatureF());
            temperature_howfeeling.setText("По ощущениям " + weatherOneDay.getTemperature_howfeelingF());
        }
        atm_pressure.setText("Давление " + weatherOneDay.getAtm_pressure());
        sun_rise.setText(weatherOneDay.getSunrise());
        sun_set.setText(weatherOneDay.getSunset());
        moon_rise.setText(weatherOneDay.getMoonrise());
        moon_set.setText(weatherOneDay.getMoonset());
        humidity.setText("Влажность " + weatherOneDay.getHumidity() + "%");
        cloudcover.setText("Облачность " + weatherOneDay.getCloudcover() + "%");
        chanceofrain.setText("Вероятность дождя " + weatherOneDay.getChanceofrain() + "%");
        chanceoffog.setText("Вероятность тумана " + weatherOneDay.getChanceoffog() + "%");
        wind.setText("Ветер " + weatherOneDay.getWind() + "м/с");
        date.setText("" + weatherOneDay.getDate());
    }

}
